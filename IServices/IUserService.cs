using bingoweb.Models;
using System.Collections.Generic;

namespace bingoweb.IServices 
{

    public interface IUserService
    {
       
        Administrator Login(Administrator oUser);
        Administrator Signup(Administrator oUser);

    }

}