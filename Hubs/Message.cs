using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
namespace bingoweb.hubs
{
    public class Message : Hub
    {
        public override Task OnConnectedAsync()
        {
            return base.OnConnectedAsync();
        }


        public Task Notification(string message){
            
            return Clients.All.SendAsync("Send",message);

        }



    }
}