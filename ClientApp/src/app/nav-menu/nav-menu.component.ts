import { Component } from '@angular/core';
import {AuthGuard} from '../auth/Auth.guard'
@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  isExpanded = false;
  public val: boolean;
  
  collapse() {
    this.isExpanded = false;
   
  }

  toggle() {
    
    this.isExpanded = !this.isExpanded;
    
  }
}
