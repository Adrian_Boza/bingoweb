import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { UsersService } from '../_services/users.service'
import { Board } from '../_models/board'
import { HubConnection, HubConnectionBuilder} from '@microsoft/signalr'
import {SignalrcustomService}  from '../service/signalrcustom.service'
@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  boards : Board[] = [];
  id: number; 
  @Input() CurrentNumber:number;
  @Input() RecentNumbers: number[] = [];
  public numbers
  public hubConection:HubConnection;

  constructor(private route:ActivatedRoute,private adminBoard:UsersService,private service:SignalrcustomService) { 
    this.route.params.subscribe(params =>{
     
      var id = Number(params['id']);
      this.adminBoard.getBoard(id).subscribe(data =>{
        this.boards.push(data);
      })

    })
  }

  ngOnInit() {
    
        this.service.emNotifica.subscribe(result =>{
          this.CurrentNumber = result;
          console.log(result)
          this.RecentNumbers.push(this.CurrentNumber);
        })
  
  }

}