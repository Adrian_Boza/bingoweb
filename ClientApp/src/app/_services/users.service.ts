
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { LoginI } from "../_models/login.interface";
import { Component, Inject } from '@angular/core';
import { Board } from "../_models/board";
import { ok } from "assert";
import { Admin } from "../_models/admin";
import { BoardAdminNumber } from '../_models/boardAdminNumber'

@Injectable({
  providedIn: "root"
})
export class UsersService {
  public animals: LoginI[];
  public showForm = false;
  url:string = "https://localhost:5001/api"
  constructor(public http: HttpClient, @Inject('BASE_URL') public baseUrl: string) {
    
  }
  
  login(form:LoginI):Observable<Admin>{
    let direction = this.url + "/administrator/Login";
    return this.http.post<Admin>(direction,form);

  }

  addBoard(form:Board):Observable<Board>{

    let direction = this.url + "/bingo/addBoard";
    form.url = "https://localhost:5001/boards/" + form.id;
    form.AdminUrl = "https://localhost:5001/AdminBoard/" + form.id;
    return this.http.post<Board>(direction,form);
  }

  delete(form:Board){

    this.http.delete(this.url + '/bingo/' + form.id).subscribe(result => {
      return ok(result);
    }, error => console.error(error));

  }

  logout (): void  {    
    localStorage.setItem ( 'isLoggedIn' , 'falso' );    
    localStorage.removeItem ( 'token' );    
    } 
  
  getBoard(id):Observable<Board>{
      
      return this.http.get<Board>('https://localhost:5001/api/bingo/getBoards/' + id);
      
  }

  addNumber(number):Observable<Board>{
  
    let direction = this.url + "/bingo/addNumber/" + number;
    return this.http.get<Board>(direction);
  }

}