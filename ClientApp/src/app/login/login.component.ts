import { Component, OnInit } from '@angular/core';
import { UsersService } from "../_services/users.service";
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { LoginI } from "../_models/login.interface";
import { Admin} from "../_models/admin";
import { Router } from '@angular/router'
import { from } from 'rxjs';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  loginForm = new FormGroup({

      email : new FormControl('',Validators.required),
      passwordHash : new FormControl('',Validators.required)

    }) 

  constructor(private userService: UsersService , private router : Router) {
  
  }
 
  ngOnInit() {

  }
  

  onLogin(form:LoginI){
    
    if(form.email != "" && form.passwordHash != ""){
      debugger
      this.userService.login(form).subscribe(data =>{ 
        let dataResponse:Admin = data;
        if(dataResponse){
          localStorage.setItem('isLoggedIn', "true");  
          localStorage.setItem('token', dataResponse.firstName + " " + dataResponse.lastName);  
          this.router.navigate(['admin']); 
          
        }
      
      }, error => console.log(error.error));
    }
     

  }
 

  











  
/* 
  loginUser(event) {
    event.preventDefault()
    const target = event.target
    var user: Login = new Login();
    user.userName = this.email
    user.password = this.password
    console.log(this.email);
    console.log(this.password);
   
    this.login(user)
  }
*/
  //login(user: Login) {

   // return this.http.post<Login>(this.baseUrl + 'api/administrator/authenticate', user).subscribe((result) => {
    //  console.log(result); });;
    
 // }
  
}
