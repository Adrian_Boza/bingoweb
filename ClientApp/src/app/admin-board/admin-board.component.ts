import { Component, Input, OnInit } from '@angular/core';
import { BoardComponent } from '../board/board.component';
import { BoardAdminNumber } from '../_models/boardAdminNumber'
import { BoardService } from '../_services/board.service'
import {SignalrcustomService} from '../service/signalrcustom.service'
import { HubConnection, HubConnectionBuilder} from '@microsoft/signalr'
import { UsersService } from '../_services/users.service'
@Component({
  selector: 'app-admin-board',
  templateUrl: './admin-board.component.html',
  styleUrls: ['./admin-board.component.css']
})
export class AdminBoardComponent  implements OnInit{
  @Input() BoardData: Array<Array<BoardAdminNumber>> = [];
  @Input() CurrentNumber:number;
  @Input() RecentNumbers: Array<number>;
  public hubConection:HubConnection;
  

  constructor(private service:BoardService, private userService:UsersService) { 
     this.BoardData = this.service.GenerateBoard();
     this.service.resetBoard();
  }

  ngOnInit(){

     
  }

  NextNumber(){
    
    this.CurrentNumber = this.service.GenerateNextNumber();
    this.RecentNumbers = JSON.parse(JSON.stringify(this.service.GetRecentNumbers(3)));
    this.userService.addNumber(this.CurrentNumber).subscribe(data =>{
      console.log(data);
    });
    
  }


 
}
