import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { Board } from '../app/_models/board'
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { ReactiveFormsModule,FormsModule} from '@angular/forms'
import { AdminComponent } from './admin/admin.component';
import { AuthGuard } from './auth/Auth.guard';
import { LoginComponent } from './login/login.component';
import { from } from 'rxjs';
import { BoardComponent } from './board/board.component'
import { Component, OnInit } from '@angular/core';
import {AdminBoardComponent} from './admin-board/admin-board.component'
@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    AdminComponent,
    LoginComponent,
    BoardComponent,
    AdminBoardComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'login', component: LoginComponent },
      { path: 'admin', component: AdminComponent,  canActivate : [AuthGuard] },
      { path: 'boards/:id', component: BoardComponent },
      { path: 'AdminBoard/:id', component: AdminBoardComponent },

    ])
  ],
  providers: [AuthGuard], 
  bootstrap: [AppComponent]
})
export class AppModule {}


