﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace bingoweb.Migrations
{
    public partial class BoardsModelUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "admin_url",
                table: "boards",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "admin_url",
                table: "boards");
        }
    }
}
